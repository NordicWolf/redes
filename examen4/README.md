# Examen 4 #
Rodríguez Coreño Aldo
30514153-6

# Configuración de redes #

Para la configuración de redes se usaron los siguientes valores

X = 1
Y = 2
Z = 3

A = 5
B = 6
C = 7

  * Equipos configurados con DHCP
    ![Laptop0][img/conf-cliente-laptop0.png]
    ![PC0][img/conf-cliente-pc0.png]

  * Equipos servidor
    ![Server2-DHCP][img/conf-servidor-server2.png]
    ![Server2-DHCP][img/conf-servidor-server2-dhcp.png]
    ![Server0-DNS][img/conf-servidor-server0.png]
    ![Server0-DNS][img/conf-servidor-server0-dns.png]
    ![Server1-HTTP][img/conf-servidor-server1.png]
    ![Server1-HTTP][img/conf-servidor-server1-http.png]

  * Otros equipos de red
    ![Printer0][img/conf-cliente-printer0.png]
    ![Printer1][img/conf-cliente-printer1.png]


# Descripción de la conexión al servidor #

De la URL dada, el cliente puede extraer información como la dirección lógica
del servidor, el puerto y el protocolo de aplicación del servicio al que se va a
conectar. En el escenario propuesto, considerando la URL
http://server1-http.gamma.local, los datos que se pueden conseguir son:

* dirección lógica: server1-http.gamma.local
* protocolo de aplicación: http
* puerto: 80

El puerto a donde se realizará la conexión puede ir de manera explícita dentro
de la URL, o bien, es obtenida usando el archivo de configuración del sistema
operativo, por ejemplo, en sistemas Unix o GNU/Linux el archivo `/etc/services`.

Para realizar la conexión, primero, el cliente debe obtener la dirección física
de red que le corresponde al servidor (IP) usando un sistema de nombres de
dominio (DNS). La aplicación cliente realiza una petición de búsqueda al
servidor DNS, en este caso, al equipo con dirección IP `172.16.3.101`. Las
peticiones y respuestas son normalmente transportadas usando el protocolo de
transporte UDP.

Cuando el cliente recibe la respuesta con la dirección física del servidor
`172.16.3.101`, genera un mensaje de aplicación con el método *GET* del
protocolo de aplicación *HTTP* con la URI del recurso que se solicita, por
ejemplo:

    GET / HTTP/1.1
    Host: server1-http.gamma.local

Después de generar el mensaje de aplicación, el cliente inicia la conexión con
el servidor mediante un servicio orientado a conexión usando un mecanismo de
*Three Way Handshake*, en que el cliente envía un segmento *TCP* con los datos
de la IP y el puerto origen (cliente), la IP y el puerto de destino (servidor) y
con la bandera *SYN* activa.

Si el servidor recibe la solicitud y es capaz de atenderla envía un segmento
*TCP* de respuesta con los datos IP y puerto origen (servidor), IP y puerto de
destino (cliente) y las banderas *SYN*, *ACK* activas. Luego, el cliente envía
el mensaje de aplicación generado previamente dentro de un nuevo segmento *TCP*
hacia el servidor.

Cada segmento va encapsulado dentro de un datagrama de red *IP*. Este datagrama
incluye entre otros datos, la versión del protocolo de red (*IPv4*), las
direcciones *IP* de origen y destino; y este datagrama, a su vez, va incluído
dentro de una trama de algún protocolo de capa de enlace, como *Ethernet*, por
ejemplo.

Como respuesta al mensaje de aplicación generado por el cliente, el servidor
envía un mensaje *HTTP* de vuelta que incluye el estado del proceso la solicitud
y en el caso de que haya sido exitoso, en el cuerpo el contenido del recurso
solicitado.

# Sumas de verificación de los paquetes IP, TCP, UDP e ICMP #

Para calcular la suma de verificación de un paquete, los octetos adyacentes
deben ser acomodados para formar enteros de 16 bits, luego se calcula la suma de
estos números y finalmente se calcula el complemento a uno al resultado de la
suma. Si al aplicar el complemento a uno alguno de los bits es cero, significa
que el paquete contiene errores.

## IP ##

Considerando los datos del encabezado IP del paquete

    4538 005d b5c2 0000 3201 ____ 1b3d 52cf
    48ef dcd1

Cálculo de la suma de verificación

    [4538] 0100010100111000
    [005d] 0000000001011101 = 0100010110010101
    [b5c2] 1011010111000010 = 1111101101010111
    [0000] 0000000000000000 = 1111101101010111
    [3201] 0011001000000001 = 10010110101011000 -> 0010110101011001
    [0000] 0000000000000000
    [1b3d] 0001101100111101 = 0001101100111101
    [52cf] 0101001011001111 = 0110111000001100
    [48ef] 0100100011101111 = 1011011011111011
    [dcd1] 1101110011010001 = 11001001111001100 -> 1001001111001101
    ===============================================================
    1100000100100110
    0011111011011001 [3ED9] < Checksum

    # ENCABEZADO TCP COMPLETO
    4538 005d b5c2 0000 3201 3ED9 1b3d 52cf
    48ef dcd1

## TCP ##

Para TCP la suma de verificación se calcula sobre los octetos que componen una
pseudo-cabecera, la cabecera de TCP y los datos de aplicación (que se deben
completar con ceros al final si es necesario).

    # ENCABEZADO IPV4
    4500 0034 6334 4000 3f06 fb90 48ef dcd1
    341d 8321
    # ENCABEZADO TCP
    208d e555 f2b5 c394 218e 24eb 8010 0166
    ____ 0000 0101 080a 3953 fd93 492b 1cb4
    ...

Según el RFC793, la pseudo-cabecera esta compuesta por la dirección IP origen,
la dirección IP destino, el número de protocolo de transporte TCP (6) y la
longitud del segmento TCP. En este caso la cabecera lleva 32 bytes y no
transporta datos.

    48ef dcd1 341d 8321 0006 0020

Cálculo de la suma de verificación, sumando la pseudo-cabecera, la cabeceta TCP
y los datos TCP. Para obtener la suma de verificación se deben sumar los
resultados obtenidos con pseudo-cabecera, la cabecera TCP y los datos (en este
caso cero) y calcular el complemento a uno al resultado.

    # PSEUDO-CABECERA
    [48ef] 0100100011101111
    [dcd1] 1101110011010001 = 10010010111000000 -> 0010010111000001
    [341d] 0011010000011101
    [8321] 1000001100100001 = 1011011100111110
    [0006] 0000000000000110 = 1011011101000100
    [0020] 0000000000100000 = 1011011101100100
    ===============================================================
                            = 1101110100100101

    # CABECERA TCP
    [208d] 0010000010001101
    [e555] 1110010101010101 = 10000010111100010 -> 0000010111100011
    [f2b5] 1111001010110101
    [c394] 1100001110010100 = 11011011001001001 -> 1011011001001010
    [218e] 0010000110001110
    [24eb] 0010010011101011 = 0100011001111001
    [8010] 1000000000010000 = 1100011010001001
    [0166] 0000000101100110 = 1100011111101111
    [0000] 0000000000000000 = 1100011111101111
    [0000] 0000000000000000 = 1100011111101111
    [0101] 0000000100000001 = 1100100011110000
    [080a] 0000100000001010 = 1101000011111010
    [3953] 0011100101010011 = 10000101001001101 -> 0000101001001110
    [fd93] 1111110110010011
    [492b] 0100100100101011 = 10100011010111110 -> 0100011010111111
    [1cb4] 0001110010110100
    ===============================================================
                            = 0010100111101111

    # SUMA DE VERIFICACIÓN
    1101110100100101
    0010100111101111 = 10000011100010100 -> 0000011100010101
    0000000000000000
    ===============================================================
    [F8EA] 01111100011101010 < Checksum

    # ENCABEZADO TCP COMPLETO
    208d e555 f2b5 c394 218e 24eb 8010 0166
    F8EA 0000 0101 080a 3953 fd93 492b 1cb4


## UDP ##

    # CABECERA IPV4
    4500 002c e78e 4000 e911 e18a 25ff 7ce7
    48ef dcd1
    # CABECERA UDP
    2713 208d 0018 ____
    # DATOS
    a7d7 8180 0001 0018 0005 000b 0566 7277

La pseudo-cabecera del mensaje se compone por dirección IP origen, dirección IP
destino, número de protocolo UDP (17), longitud del segmento UDP (24).

    [25ff] 0010010111111111
    [7ce7] 0111110011100111 = 1010001011100110
    [48ef] 0100100011101111 = 1110101111010101
    [dcd1] 1101110011010001 = 11100100010100110 -> 1100100010100111
    [0011] 0000000000010001
    [0018] 0000000000011000 = 0000000000101001
    ===============================================================
    [C8D0] 1100100011010000

    # CABECERA UDP
    [2713] 0010011100010011
    [208d] 0010000010001101 = 0100011110100000
    [0018] 0000000000011000 = 0100011110111000
    [0000] 0000000000000000 = 0100011110111000
    ===============================================================
    [47B8] 0100011110111000

    # DATOS UDP
    [a7d7] 1010011111010111
    [8180] 1000000110000000 = 10010100101010111 -> 0010100101011000
    [0001] 0000000000000001
    [0018] 0000000000011000 = 0000000000011001
    [0005] 0000000000000101 = 0000000000011110
    [000b] 0000000000001011 = 0000000000101001
    [0566] 0000010101100110 = 0000010110001111
    [7277] 0111001001110111 = 0111100000000110
    ===============================================================
    [A15E] 1010000101011110

La suma de verificación se obtiene sumando los valores obtenidos en las sumas
parciales.

    # SUMA DE VERIFICACIÓN
    1100100011010000
    0100011110111000 = 10001000010001000 -> 0001000010001001
    1010000101011110
    ===============================================================
    [B1E7] 1011000111100111
    [4E18] 0100111000011000

    # CABECERA UDP COMPLETA
    2713 208d 0018 4E18
