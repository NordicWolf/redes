# Tareas #

Nombre:		Aldo Rodríguez Coreño
No. Cuenta:	30514153-6
Correo:		aldo.rc@ciencias.unam.mx

# Tarea HTTP #

+ ¿Existen diferencias entre los archivos?, ¿Cuáles?
  Sí, cambiaron algunas lineas de scripts

+ ¿Cambia la respuesta del servidor cuando se pasa la _cookie_ en la petición?
  No

+ ¿Por qué se envian las _cookies_ en la petición al servidor?
  Para mantener el estado de la sesión durante la conexión al servidor

# Tarea SSL #

La ruta /etc/ssl/certs es un directorio de certificados de confianza. Contiene
certificados firmados por una *Autoridad de Certificación* y permiten verificar
la integridad y la propiedad de una llave pública.

El archivo `chain.pem` es un certificado adicional que contiene varios
certificados en formato PEM concatenados.

La validación del certificado se realiza calculando el hash del contenido del
certificado `www_nokia_com.pem` y lo compara con el hash descifrado usando la
llave pública de la CA contenida en `chain.pem` o alguno de los certificados en
/etc/ssl/certs. Si los hashes coinciden se puede asumir que el certificado y la
llave pública que contiene no han sido alterados y proviene del servidor
(nokia.com).
